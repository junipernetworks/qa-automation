Scope of this project is to automate the links/urls present in the juniper.net pages.
1) To verify the response code of all urls on the web page
2) To verify the target window where the link opens for all urls on the web page

There are 2 types of pages on juniper.net, one that is in old format(html code) and other which is new format (json components)

There are two test methods created in this project using testng annotation @test
1) To verify all the links on the main content or body of the page
2) To verify only the headers and footers of required web pages

Steps To run this automation script:
1) To verify all the links on the main content or body of the page
    a) Enter the list of urls to test in the ListOfUrls.txt file which is hosted in the input folder of the project.
    (Enter one url per line only). You can enter links to web pages that are in both old and new format
    b) Uncomment the line <class name="scripts.ReadInputFile" /> in testng.xml file
    c) Right click on testng.xml > Run > Run as TestNG suite
    d) Output will be captured in emailabale-report.html file of test-output folder
    
2) To verify only the headers and footers of required web pages
	a) Open the source file gerneric.TestHeaderFooter.java
	b) Enter the target links to test in the dataprovider method urls()
	c) Uncomment the line <class name="generic.TestHeaderFooter" /> in the testng.xml file
	d) Right click on testng.xml > Run > Run as TestNG suite
    e) Output will be captured in emailabale-report.html file of test-output 
    

