package scripts;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.json.Json;
import org.testng.Reporter;

import com.google.gson.JsonNull;

import fetchUrls.FetchAllJSONComponents;
import fetchUrls.FetchAllUrls;
import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class FetchComponentsFromJSON {

	// This class is used to fetch all the json components on a web page

	public static void fetchCompoenents(String json_src, WebDriver driver, String url, String pageSrc) {
		JSONParser parser = new JSONParser();
		
		// JSONParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES
		FetchAllJSONComponents fjson = new FetchAllJSONComponents();
		try {

			// VerifyTargetForLinks vfrl = new VerifyTargetForLinks();
			// VerifyHttpErrors vhttpe = new VerifyHttpErrors();
			//System.out.println(json_src.substring(5726));
			JSONObject compObj;
			//Reporter.log(json_src);
			Object obj = parser.parse(json_src);
			
			
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray components = (JSONArray) jsonObject.get("components");

			for (int i = 0; i < components.size(); i++) {
				JSONObject eachComponent = (JSONObject) components.get(i);
				String component_name = eachComponent.get("selector").toString();
				compObj = (JSONObject) eachComponent.get("properties");
				// Product n solution page components
				if (component_name.equals("sw-marquee")) {
					fjson.swmarqueeComponent(compObj, url);
				} else if (component_name.equals("sw-breadcrumb")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-product-category")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-mosaic-static")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-ticker")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-quick-tasks")) {
					fjson.swQuickTasksComponent(compObj, url);
				} else if (component_name.equals("sw-product-overview")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-partner-links")) {					
					fjson.allPageComponent(component_name, url, driver);
				}
				// product comapare page component
				else if (component_name.equals("sw-product-compare")) {
					fjson.compareProductsPage(compObj, url, driver);
				}
				// product specification page component
				else if (component_name.equals("sw-product-spec-sheet")) {
					fjson.allPageComponent(component_name, url, driver);
				}

				// hero marquee slide component
				else if (component_name.equals("sw-marquee-slides")) {
					fjson.swMarqueeSlides(compObj, url, driver);
				}

				// carousel component
				else if (component_name.equals("sw-related-nav-links")) {
					fjson.allPageComponent(component_name, url, driver);
				}

				// featured products component
				else if (component_name.equals("sw-notes")) {
					fjson.allPageComponent(component_name, url, driver);
				}

				// training page component
				else if (component_name.equals("sw-subnav")) {
					fjson.swSubNavComponent(compObj, url);
				} else if (component_name.equals("sw-list-grid")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-editorial")) {
					fjson.swEditorialComponent(compObj, url, driver, pageSrc);
				}else if (component_name.equals("sw-table")) {
					fjson.swTableComponent(compObj, url, driver);
				}else if (component_name.equals("sw-dual-info")) {
					fjson.swDualInfoComponent(compObj, url, driver);
				}
				
				
				// https://origin-www.juniper.net/us/en/training/jnbooks/
				else if (component_name.equals("sw-training-books")) {
					fjson.swTrainingBooksComponent(compObj, url, driver);
				}
				
				// https://origin-www.juniper.net/us/en/solutions/automation/programmability/
				else if (component_name.equals("sw-editorial-pairing")) {
					fjson.allPageComponent(component_name, url, driver);
				} else if (component_name.equals("sw-link-cards")) {
					fjson.linkCardsComponent(compObj, url, driver);
				}

				//for dm page
				//https://uat.juniper.net/us/en/dm/sd-wan/
				else if (component_name.equals("sw-banner")) {
					fjson.swbannerComponent(compObj, url, driver);
				}
				
				fjson.getErrorCount();
			}

			int count = fjson.getCount();
			Reporter.log("<div class=\"count\">" + "Number of urls: " + count + "</div>");

			Reporter.log(
					"<div class=\"total\">" + "Total 404 issues on page = " + fjson.getFail_404errors() + "</div>");

			Reporter.log("<div class=\"total\">" + "Total target link failures on page = "
					+ fjson.getFail_count_target() + "</div>");

			Reporter.log("<div class=\"total\">" + "Total number of links to be verified manually = "
					+ fjson.getManualVerificationCount() + "</div>");

			StringBuilder outputLog = fjson.getOutputLog();
			Reporter.log(outputLog.toString());

		} catch (org.json.simple.parser.ParseException e) {
			Reporter.log("Unexpected Character error - issue with json component", true);			
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
