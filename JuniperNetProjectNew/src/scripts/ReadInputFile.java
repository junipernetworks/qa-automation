package scripts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import fetchUrls.FetchAllUrls;
import junit.framework.Assert;
import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class ReadInputFile extends BaseTest {
	boolean firstTime = true;

	@Test
	public void readInputFile() {
		BufferedReader br = null;
		int numberOfLines = 0;
		String eachUrl;
		String entirePageSrc;
		String workingDir = System.getProperty("user.dir");
		
		try {
			File file = new File("C:\\Juniper.NetAutomation\\Input\\ListOfUrls.txt");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String sCurrentLine;

			if ((file.length()) == 0) {
				Reporter.log("No urls present in file, enter the urls to test", true);
			}

			ArrayList<String> urlsToTest = new ArrayList<String>();

			while ((sCurrentLine = br.readLine()) != null) {

				if (sCurrentLine.isEmpty())
					continue;

				urlsToTest.add(sCurrentLine.trim().replaceAll("\uFFFD", ""));
				
				numberOfLines++;

			}
			System.out.println("number of urls to test" + numberOfLines);

			Iterator itr = urlsToTest.iterator();

			Reporter.log("<br>");
			Reporter.log("<div class=\"container\">");

			Reporter.log("<div class=\"tabs\" data-tabsize=\"" + urlsToTest.size() + "\"></div>");
			int count = 0;
			Reporter.log("<div class=\"tabBody\">");
			while (itr.hasNext()) {
				Reporter.log("<div class=\"page\" data-pageid=\"pg" + (++count) + "\">");
				eachUrl = itr.next().toString().trim();
				Reporter.log("<div class=\"msgTitle\">" + "<a href=" + eachUrl + " target=\"_blank\">" + "Url: "
						+ eachUrl + "</a></div>");
				Reporter.log("<br>");
				VerifyHttpErrors httperror = new VerifyHttpErrors();
				String flag = httperror.verifyFor404Errors(eachUrl, eachUrl);

				if (flag.equalsIgnoreCase("test url leads to 404 page not found error"))
					break;

				VerifyTargetForLinks targeterrors = new VerifyTargetForLinks();
				targeterrors.verifyTargetWindow(eachUrl, eachUrl, "");
				driver.get(eachUrl);
				Reporter.log("Title of page : "+ driver.getTitle());
								
				System.out.println(eachUrl);
				getPagesource(eachUrl);
				Reporter.log("</div>");

			}
			Reporter.log("</div>");
			Reporter.log("</div>");
			if (firstTime) {
				Reporter.log(
						"<script type=\"text/javascript\" src=\"https://www.juniper.net/documentation/test/POC/dme-qa/scripts/jquery.min.js\"></script>");

				// To add css and javascript to output html file
				outputhtmlformat.CSSandJavaScript.AddCssAndJStoOutputHTMLFile("/styles/cssStyles.txt");
				outputhtmlformat.CSSandJavaScript.AddCssAndJStoOutputHTMLFile("/javascript/jsJavaScript.txt");
				firstTime = !firstTime;
			}

		} catch (WebDriverException e) {
			String errMsg = e.getMessage();
			if (errMsg.contains("chrome not reachable")
					|| errMsg.contains("no such window: target window already closed")) {
				System.out.println("Browser was closed by user during execution..try again");
			} else
				System.out.println(e.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void getPagesource(String eachUrl) throws WebDriverException {

		StringBuilder pageSrc = new StringBuilder();
		String line;
		String pageData = null;
		FetchAllUrls furls = new FetchAllUrls();
		VerifyHttpErrors vError = new utility.VerifyHttpErrors();
		VerifyTargetForLinks vLinks = new utility.VerifyTargetForLinks();
				
		try {
			if (eachUrl.contains("http:"));
			eachUrl = eachUrl.replace("http:", "https:");			
			
			String chk_error_404 = vError.verifyFor404Errors(eachUrl, eachUrl);
			Reporter.log(chk_error_404);	
			
			
			if(eachUrl.contains("/dm/"))
				Reporter.log("<h4> Verify the videos on the page manually,if any </h4>");					
 
			//initForSslConnection();
			
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
 
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
 
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
 
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
						
			URL url = new URL(eachUrl);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));			
					
			while ((line = br.readLine()) != null) {				
					pageSrc.append(line);
			}

			String fullPageSrc = pageSrc.toString();
			
					
		
			br.close();
			
			String json_tag = "_PAGE_DATA =";

			if (fullPageSrc.contains(json_tag)) {
				//pageData = pageSrc.substring(pageSrc.indexOf("_PAGE_DATA") + json_tag.length(),pageSrc.indexOf("};") + 1);
				String startStr = pageSrc.substring(pageSrc.indexOf("_PAGE_DATA") + json_tag.length());
				int startIndex = pageSrc.indexOf(startStr);
				int endIndex = pageSrc.indexOf("};", startIndex);
				pageData = pageSrc.substring(startIndex, endIndex);
				
				pageData = pageData + " }";						
				pageData = verifyJsonStructure(pageData);
				FetchComponentsFromJSON.fetchCompoenents(pageData, driver, eachUrl, pageData);
			}

			else {
				pageData = fullPageSrc;
				furls.fetchUrls(eachUrl, driver);
			}

		} catch (MalformedURLException e) {
			Reporter.log(eachUrl + "url is malformed");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			Reporter.log("Invalid url or hostname");
		}catch(SSLHandshakeException e){
			Reporter.log("cannot handle origin-stage urls");
		}catch (IOException e) {
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		} 

	}

	private String verifyJsonStructure(String jsonPageData) {
		jsonPageData = jsonPageData.replaceAll("\r\n|\n|\r|\t|  ","");
		jsonPageData = jsonPageData.replaceAll(",,", ",");
		jsonPageData = jsonPageData.replaceAll(", ,", ",");
		jsonPageData = jsonPageData.replaceAll(",]", "]");
		jsonPageData = jsonPageData.replaceAll(", ]", "]");
		jsonPageData = jsonPageData.replaceAll(",}", "}");
		jsonPageData = jsonPageData.replaceAll(", }", "}");
		jsonPageData = jsonPageData.replaceAll("'<", "\"<");
		jsonPageData = jsonPageData.replaceAll(">'", "<\"");
		jsonPageData = jsonPageData.replaceAll("};", "}");		
		return jsonPageData;
	}

	public static void initForSslConnection ()	{
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = null;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			 
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
		}catch (KeyManagementException e) {
			throw new RuntimeException(e);
		}catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	
}
