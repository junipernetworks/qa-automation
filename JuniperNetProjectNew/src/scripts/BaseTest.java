package scripts;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.TestRunner;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class BaseTest {
	
	public  WebDriver driver;
	
	@BeforeTest
	public void setup(ITestContext ctx) {
		
		String sdfDate = new SimpleDateFormat("ddMMMyyyy_HHmm").format(Calendar.getInstance().getTime());
			
		TestRunner runner = (TestRunner) ctx;
		
	    String outputPath = "C:/Juniper.NetAutomation/TestResults/"+ sdfDate;
	  
	    runner.setOutputDirectory(outputPath);	    
	    
	}
	
	@BeforeMethod
		@Parameters("browser")
		public void openBrowser(String browser){
		
		System.out.println("Inside openbrowser");
			String workingDir = System.getProperty("user.dir");
		
					//Check if parameter passed from TestNG is 'firefox'
					if(browser.equalsIgnoreCase("firefox")){
					//create firefox instance
						Reporter.log("executing in firefox",true);
						System.setProperty("webdriver.gecko.driver", workingDir+"/exes/geckodriver.exe");
						driver = new FirefoxDriver();
					}
					
					// Check if parameter passed from TestNG is 'ie11'
					else if (browser.equalsIgnoreCase("ie11")) {
						// create ie11instance
						Reporter.log("executing in ie11",true);
						System.setProperty("webdriver.ie.driver", workingDir+"/exes/IEDriverServer.exe");
						driver = new InternetExplorerDriver();
					}
					
					//Check if parameter passed as 'chrome'
					else if(browser.equalsIgnoreCase("chrome")){
						//set path to chromedriver.exe
						Reporter.log("executing in chrome",true);
						/*DesiredCapabilities capability = DesiredCapabilities.chrome();
						capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);*/
						System.setProperty("webdriver.chrome.driver",workingDir+"/exes/chromedriver.exe");
						driver = new ChromeDriver();
						driver.manage().window().maximize();
						// driver = new ChromeDriver();
					}
					
					//System.out.println(driver == null);
					
					
		}
	
	public WebDriver getDriver(){
		return this.driver;
	}
	
	@AfterMethod
	public void closeBrowser(){
		driver.close();
	}

	
}
