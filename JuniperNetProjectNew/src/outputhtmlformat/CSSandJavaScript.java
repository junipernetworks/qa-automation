package outputhtmlformat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.testng.Reporter;

public class CSSandJavaScript {

	public static void AddCssAndJStoOutputHTMLFile(String path) {
		// to add css to html file
		String workingDir = System.getProperty("user.dir");
		String filePath = workingDir + path;
		StringBuilder stbr = new StringBuilder();
		try {
			BufferedReader bfr1 = new BufferedReader(new FileReader(filePath));
			String readLine;
			while ((readLine = bfr1.readLine()) != null) {
				if (readLine.isEmpty())
					continue;
				stbr.append(readLine);
			}
			String cssJs = stbr.toString();
			Reporter.log(cssJs);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
