package utility;

import org.testng.Reporter;

public class VerifyTargetForLinks {
	// This class is used for verifying the targets where the link opens for the
	// given urls
	private int fail_count_target = 0;
	private int verify_manual_count = 0;

	public String verifyTargetWindow(String curr_url, String test_url, String target) {
		try {
			target = target.trim();
			String start = null;
			// to get a well formed url
			if ((curr_url.startsWith("http://juniper.net"))
					|| (curr_url.startsWith("https://juniper.net")) && (curr_url.contains("support"))) {
				curr_url = curr_url.replace("http://juniper.net", "http://www.juniper.net");
			}
			if (test_url.startsWith("http://www.juniper.net") || test_url.startsWith("https://www.juniper.net"))
				start = "https://www.juniper.net";

			else if (test_url.startsWith("http://origin-www.juniper.net")
					|| test_url.startsWith("https://origin-www.juniper.net"))
				start = "https://origin-www.juniper.net";

			else if (test_url.startsWith("http://stage.juniper.net")
					|| test_url.startsWith("https://stage.juniper.net"))
				start = "https://stage.juniper.net";

			else if (test_url.startsWith("http://origin-stage.juniper.net")
					|| test_url.startsWith("https://origin-stage.juniper.net"))
				start = "https://origin-stage.juniper.net";

			else if (test_url.startsWith("http://uat.juniper.net") || test_url.startsWith("https://uat.juniper.net"))
				start = "https://uat.juniper.net";

			if (curr_url.startsWith("/")) {
				curr_url = start + curr_url;
			}

			if (curr_url.contains("https://www.juniper.net/certmanager")
					|| curr_url.contains("https://stage.juniper.net/certmanager/")
					|| curr_url.contains("https://uat.juniper.net/certmanager")
					|| curr_url.contains("https://origin-www.juniper.net/certmanager")
					|| curr_url.contains("https://origin-stage.juniper.net/certmanager")) {
				curr_url = "https://www.certmetrics.com/juniper/login.aspx?ReturnUrl=%2fjuniper";
				
			}

			if (curr_url.contains("company/careers/job-search/")) {
				curr_url = "https://careers.juniper.net/careers/careers/";
			}

			if (curr_url.contains("elqNow/elqRedir.htm?ref=")) {
				int index = curr_url.indexOf("=");
				curr_url = curr_url.substring(index + 1);
			}

			if (curr_url.contains("training/open-learning/")) {
				curr_url = "https://openlearning.juniper.net/";
			}
			
			if (curr_url.startsWith("tel:")) {
				verify_manual_count++;
				return (printTargetVerifyManualFormat(curr_url, target, "telephone link verify manually"));
			} else if (curr_url.equals("javascript://chat")) {
				verify_manual_count++;
				return (printTargetVerifyManualFormat(curr_url, target, "verify live chat popup manually"));
			} else if (curr_url.startsWith("http://origin-www.juniper.net")
					|| (curr_url.startsWith("https://origin-www.juniper.net"))
					|| (curr_url.startsWith("http://www.juniper.net"))
					|| (curr_url.startsWith("https://www.juniper.net"))
					|| (curr_url.startsWith("http://kb.juniper.net")) || (curr_url.startsWith("https://kb.juniper.net"))
					|| (curr_url.startsWith("http://stage.juniper.net"))
					|| (curr_url.startsWith("https://stage.juniper.net"))
					|| (curr_url.startsWith("http://origin-stage.juniper.net"))
					|| (curr_url.startsWith("https://origin-stage.juniper.net"))
					|| (curr_url.startsWith("http://uat.juniper.net"))
					|| (curr_url.startsWith("https://uat.juniper.net")))

			{
				if ((curr_url.endsWith(".html"))
						&& ((curr_url.contains("techpubs")) || (curr_url.contains("documentation")))) {
					if (target.equals("_blank") || target.contains("new")) {
						fail_count_target++;
						return (printTargetFailFormat(curr_url, target, "internal link: techpubs"));
					} else
						return (printTargetPassFormat(curr_url, target, "internal link"));

				} else if (curr_url.endsWith(".html")) {
					if (target.equals("_blank") || target.contains("new")) {
						return (printTargetPassFormat(curr_url, target, "html link"));						
					} else {
						fail_count_target++;
						return (printTargetFailFormat(curr_url, target, "html link"));
					}

				}

				else if ((curr_url.endsWith(".mp3")) || (curr_url.endsWith(".png"))|| (curr_url.endsWith(".jpg"))) {
					if (target.equals("_blank") || target.contains("new")) {
						return (printTargetPassFormat(curr_url, target, "mp3 or iamge link"));											
					} else {
						fail_count_target++;
						return (printTargetFailFormat(curr_url, target, "mp3 or image link"));	
					}
				}

				else if ((curr_url.endsWith(".pdf"))) {
					if (target.equals("_blank") || target.contains("new")) {
						return (printTargetPassFormat(curr_url, target, "pdf link"));

					} else if ((curr_url.endsWith(".html")) && (curr_url.contains("assets"))) {
						if (target.equals("_blank") || target.contains("new")) {
							return (printTargetPassFormat(curr_url, target, "pdf/html link"));

						}
					} else {
						fail_count_target++;
						return (printTargetFailFormat(curr_url, target, "pdf/html link"));
					}
				} else if (target.equals("_blank") || target.contains("new")) {
					if (curr_url
							.equals("https://www.juniper.net/assets/us/en/local/ebooks/more-wallet-share-made-easy/")) {
						verify_manual_count++;
						return (printTargetVerifyManualFormat(curr_url, target, "verify manually"));
					} else {
						fail_count_target++;
						return (printTargetFailFormat(curr_url, target, "internal link"));

					}
				} else
					return (printTargetPassFormat(curr_url, target, "internal link"));

			} else if (((curr_url.startsWith("https://www.you")) || (curr_url.startsWith("https://player.youku.com/"))
					|| (curr_url.startsWith("https://you")))) {
				verify_manual_count++;
				return (printTargetVerifyManualFormat(curr_url, target, "video link - verify manually"));
			} else if (curr_url.contains("mailto")) {
				verify_manual_count++;
				return (printTargetVerifyManualFormat(curr_url, target, "mail link - verify manually"));
			} else if (curr_url.startsWith("javascript:void(0)")) {
				verify_manual_count++;
				return (printTargetVerifyManualFormat(curr_url, target, "verify manually if link visible else ignore"));
			} else if (curr_url.equals(""))
				return ("");
			else if (target.equals("_blank") || target.contains("new") || target.equals("blank"))
				return (printTargetPassFormat(curr_url, target, "external link"));

			else {
				fail_count_target++;
				return (printTargetFailFormat(curr_url, target, "external link"));
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getTargetCount() {
		return fail_count_target;
	}

	public int getManualVerificationCount() {
		return verify_manual_count;
	}

	private String printTargetPassFormat(String curr_url, String target, String output_text) {
		if (target.equals(""))
			target = "\"\"";
		String myVar = "<table class = \"tableall\">" + "<tr>" + "<td class = \"tdurl\">" + curr_url + "</td>"
				+ "<td class = \"tdresptgt\"> " + "Target : " + target + "</br>" + output_text + "</td>"
				+ "<td class = \"tdtargetpass\"> Status: Pass </td>" + "</tr>" + "</table>";
		return (myVar);

	}

	private String printTargetFailFormat(String curr_url, String target, String output_text) {
		if (target.equals(""))
			target = "\"\"";
		String myVar = "<table class = \"tableall\">" + "<tr>" + "<td class = \"tdurl\">" + curr_url + "</td>"
				+ "<td class = \"tdresptgt\"> Target :" + target + "</br>" + output_text + "</td>"
				+ "<td class = \"tdtargetfail\"> Status: Fail " + "</td>" + "</tr>" + "</table>";
		return (myVar);

	}

	private String printTargetVerifyManualFormat(String curr_url, String target, String output_text) {
		if (target.equals(""))
			target = "\"\"";
		String myVar = "<table class = \"tableall\">" + "<tr>" + "<td class = \"tdurl\">" + curr_url + "</td>"
				+ "<td class = \"tdresptgt\"> Target:" + target + "</br>" + output_text + "</td>"
				+ "<td class = \"tdtargetmanual\"> Status: Unknown " + "</br>" + "</td>" + "</tr>" + "</table>";
		return (myVar);

	}
}