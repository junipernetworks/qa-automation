package utility;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;
import java.util.Scanner;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import fetchUrls.FetchAllJSONComponents;
import scripts.BaseTest;

public class VerifyHttpErrors<driver> {
	private int fail_404errors = 0;
	BaseTest bc = new BaseTest();
	WebDriver driver = bc.getDriver();

	public String verifyFor404Errors(String curr_url, String test_url) {
		int resp_code = 0;

		try {

			String start = null;

			if (curr_url.contains("elqNow/elqRedir.htm?ref=")) {
				int index = curr_url.indexOf("=");
				curr_url = curr_url.substring(index + 1);

			}

			// to get a well formed url
			if ((test_url.startsWith("http://www.juniper.net")) || (test_url.startsWith("https://www.juniper.net")))
				start = "https://www.juniper.net";

			else if ((test_url.startsWith("http://origin-www.juniper.net"))
					|| (test_url.startsWith("https://origin-www.juniper.net")))
				start = "https://origin-www.juniper.net";

			else if ((test_url.startsWith("http://stage.juniper.net"))
					|| (test_url.startsWith("https://stage.juniper.net")))
				start = "https://stage.juniper.net";

			else if ((test_url.startsWith("http://origin-stage.juniper.net"))
					|| (test_url.startsWith("https://origin-stage.juniper.net")))
				start = "https://origin-stage.juniper.net";

			else if ((test_url.startsWith("http://uat.juniper.net"))
					|| (test_url.startsWith("https://uat.juniper.net")))
				start = "https://uat.juniper.net";

			if (curr_url.startsWith("/")) {
				curr_url = start + curr_url;
			}

			if (curr_url.contains("company/careers/job-search/")) {
				curr_url = "https://careers.juniper.net/careers/careers/";
			}

			if (curr_url.contains("training/open-learning/")) {
				curr_url = "https://openlearning.juniper.net/";
			}

			if (curr_url.contains("https://www.juniper.net/certmanager")
					|| curr_url.contains("https://stage.juniper.net/certmanager/")
					|| curr_url.contains("https://uat.juniper.net/certmanager")
					|| curr_url.contains("https://origin-www.juniper.net/certmanager")
					|| curr_url.contains("https://origin-stage.juniper.net/certmanager")) {
				curr_url = "https://www.certmetrics.com/juniper/login.aspx?ReturnUrl=%2fjuniper";

			}

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(curr_url);
			HttpResponse response = client.execute(request);
			resp_code = response.getStatusLine().getStatusCode();

			/*
			 * URL url = new URL(curr_url); HttpURLConnection connection =
			 * (HttpURLConnection) url.openConnection(); connection.connect();
			 * resp_code = connection.getResponseCode();
			 * System.out.println("resp code:"+ resp_code);
			 */

			/*
			 * org.jsoup.Connection.Response response = null; response =
			 * Jsoup.connect(curr_url).timeout(10000).execute(); resp_code =
			 * response.statusCode(); //System.out.println("url is:"+ curr_url);
			 * System.out.println("resp code:"+ resp_code);
			 */

			if ((curr_url.equals(test_url)) && (resp_code != 200)) {
				return (print404FailFormat(curr_url, resp_code,
						"Given url does not exist, please check if you have entered the correct url"));

			}

			else if (resp_code != 200) {
				// System.out.println("response code" + resp_code);
				fail_404errors++;
				// System.out.println(curr_url);
				return (print404FailFormat(curr_url, resp_code));

			} else {

						 
				Document doc = Jsoup.connect(curr_url).timeout(10000).get();
				String page_title = doc.title();

				if ((page_title.contains("Fehler")) || (page_title.contains("erreur")) || (page_title.contains("错误"))
						|| (page_title.contains("오류")) || (page_title.contains("エラー"))
						|| (page_title.contains("404 : Page not found"))) {
					fail_404errors++;
					return (print404FailFormat(curr_url, resp_code, "Status:Fail, soft 404 error"));
				}

				else
					return (print404PassFormat(curr_url, resp_code));
			}

		} catch (Exception e) {

		}
		// return false;
		return ("");
	}

	public int get404Count() {
		return fail_404errors;
	}

	private String print404PassFormat(String curr_url, int resp_code) {
		String myVar = "<table class = \"tableall\"><tr><td class = \"tdurl\">" + curr_url
				+ "</td><td class = \"tdresptgt\"> Resp code: " + resp_code
				+ "</td><td class = \"td404pass\"> Status: Pass </td></tr></table>";

		return (myVar);
	}

	private String print404FailFormat(String curr_url, int resp_code) {
		String myVar = "<table class = \"tableall\">" + "<tr>" + "<td class = \"tdurl\">" + curr_url + "</td>"
				+ "<td class = \"tdresptgt\"> Resp code:" + resp_code + "</td>"
				+ "<td class = \"td404fail\"> Status: Fail </td>" + "</tr>" + "</table>";
		return (myVar);

	}

	private String print404FailFormat(String curr_url, int resp_code, String msg) {
		String myVar = "<table class = \"tableall\">" + "<tr>" + "<td class = \"tdurl\">" + curr_url + "</td>"
				+ "<td class = \"tdresptgt\"> Resp code:" + resp_code + "</td>" + "<td class = \"td404fail\">" + msg
				+ " </td>" + "</tr>" + "</table>";
		return (myVar);

	}

}
