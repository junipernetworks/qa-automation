package fetchUrls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import scripts.BaseTest;
import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class FetchAllUrls {
	VerifyHttpErrors vError = new VerifyHttpErrors();
	VerifyTargetForLinks vTarget = new VerifyTargetForLinks();
	StringBuilder outputLog = new StringBuilder();
	String logText = "";
	private int fail_count_target = 0;
	private int manual_verification_count = 0;
	private int fail_404errors = 0;

	public void fetchUrls(String test_url, WebDriver driver) {
		// This method fetches all urls in the main content of page excluding
		// headers and footers
		// This is called only for juniper.net pages that are in old format

		String target, label;
		driver.get(test_url);
		WebElement content = driver.findElement(By.id("content"));

		List<WebElement> allLinks = content.findElements(By.tagName("a"));
		Reporter.log("<div class=\"count\">" + "Total number of links : " + allLinks.size() + "</div>");

		for (WebElement e : allLinks) {

			label = e.getAttribute("innerText");
			if (label.equals(""))
				label = e.getAttribute("title");
			outputLog.append("<div class = \"label\">" + label + "</div>");

			String curr_link;
			curr_link = e.getAttribute("href");

			// added for dm pages where the link is given in onclick attribute
			// of 'a' tag
			// instead of href
			String video_overlay = e.getAttribute("class");
			String data_source = e.getAttribute("data-source");
			if (curr_link == null) {
				curr_link = e.getAttribute("onclick");
				if (curr_link != null && curr_link.contains("http")) {
					int beginIndex = curr_link.indexOf("http");
					curr_link = curr_link.substring(beginIndex);
					curr_link = curr_link.replaceAll("[();']", "");

				}
				
				//added for the video in the url
				//https://uat.juniper.net/us/en/dm/build-more/security/
				else if (curr_link == null && video_overlay.equals("video-overlay-trigger")
						&& data_source.equals("youtube")) {
					curr_link = "https://www.youtube.com/watch?v=" + e.getAttribute("data-video-id");
				}
			}

			if (curr_link == null)
				curr_link = "";
			logText = vError.verifyFor404Errors(curr_link, test_url);
			outputLog.append(logText);
			if ((e.getAttribute("target")) == null)
				target = "";
			else
				target = e.getAttribute("target");

			logText = vTarget.verifyTargetWindow(curr_link, test_url, target);
			outputLog.append(logText);
		}

		getErrorCount();

		Reporter.log("<div class=\"total\">" + "Total 404 issues on page = " + getFail_404errors() + "</div>");

		Reporter.log(
				"<div class=\"total\">" + "Total target link failures on page = " + getFail_count_target() + "</div>");

		Reporter.log("<div class=\"total\">" + "Total number of links to be verified manually = "
				+ getManualVerificationCount() + "</div>");

		Reporter.log(outputLog.toString());
	}

	public int getFail_count_target() {
		return fail_count_target;
	}

	public void setFail_count_target(int fail_count_target) {
		this.fail_count_target = fail_count_target;
	}

	public int getFail_404errors() {
		return fail_404errors;
	}

	public void setFail_404errors(int fail_404errors) {
		this.fail_404errors = fail_404errors;
	}

	public int getManualVerificationCount() {
		return manual_verification_count;
	}

	public void getErrorCount() {
		fail_404errors = vError.get404Count();
		fail_count_target = vTarget.getTargetCount();
		manual_verification_count = vTarget.getManualVerificationCount();
	}

}
