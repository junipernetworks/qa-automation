package fetchUrls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class FetchAllJSONComponents {

	// This class hosts methods for fetching and validating all urls in each
	// json component of the page
	// This does not include components for header and footer
	// This is valid only for the juniper.net pages that are in new format (with
	// json components)

	int count = 0;
	int editorial_count = 1;
	int mosaic_tile_counter = 1;
	int prod_cate_counter = 1;
	int sw_notes_counter = 1;
	StringBuilder outputLog = new StringBuilder();
	String logText = "";
	private int fail_count_target = 0;
	private int fail_404errors = 0;
	private int manual_verification_count = 0;
	VerifyHttpErrors vErrorObj = new VerifyHttpErrors();
	VerifyTargetForLinks vTargetObj = new VerifyTargetForLinks();

	private void subComponent(JSONObject items, String test_url) {
		String url = "";
		String target;
		String label;
		String title;

		if ((items.get("url")) == null || items.get("url").toString().equals(""))
			url = "";
		else
			url = items.get("url").toString();

		if ((items.get("target")) == null)
			target = "";
		else
			target = items.get("target").toString();

		if ((items.get("label")) == null)
			label = "";
		else
			label = items.get("label").toString();

		label = editLabel(label);

		if ((items.get("title")) == null)
			title = "";
		else
			title = items.get("title").toString();

		if ((items.get("cta")) == null)
			title = "";
		else
			title = items.get("cta").toString();

		if (url != null && !url.equals("")) {
			outputLog.append(label);
			outputLog.append(title);
			logText = vErrorObj.verifyFor404Errors(url, test_url);
			outputLog.append(logText);
			logText = vTargetObj.verifyTargetWindow(url, test_url, target);
			outputLog.append(logText);
			count++;
		}
	}

	public void swmarqueeComponent(JSONObject properties, String test_url) {
		// added for pages similar to
		// https://stage.juniper.net/fr/fr/products-services/what-is/multi-access-edge-computing/

		JSONObject logoObj = (JSONObject) properties.get("logo");
		if (logoObj != null) {
			outputLog.append("<h3>Juniper Logo</h3>");
			subComponent(logoObj, test_url);

		}

		outputLog.append("<h3>sw-marquee</h3>");
		JSONObject actionObj = (JSONObject) properties.get("action");
		subComponent(actionObj, test_url);

		// added for the url
		// https://stage.juniper.net/fr/fr/products-services/what-is/multi-access-edge-computing/

		JSONArray marqueeLinks = (JSONArray) properties.get("aside");
		if (marqueeLinks != null) {
			outputLog.append("<h4>links within sw-marquee</h4>");
			for (int i = 0; i < marqueeLinks.size(); i++) {
				JSONObject asideObj = (JSONObject) marqueeLinks.get(i);
				subComponent(asideObj, test_url);

			}
		}
	}

	public void swQuickTasksComponent(JSONObject properties, String test_url) {
		outputLog.append("<h3>Resource bar tiles</h3>");
		JSONArray swQuickTasksArray = (JSONArray) properties.get("items");

		String url="";
		String target;
		String label1, label2;
		for (int i = 0; i < swQuickTasksArray.size(); i++) {
			JSONObject items = (JSONObject) swQuickTasksArray.get(i);
			label1 = items.get("label").toString();
			if (items.containsKey("items")) {
				JSONArray itemsSubArray = (JSONArray) items.get("items");
				for (int j = 0; j < itemsSubArray.size(); j++) {
					items = (JSONObject) itemsSubArray.get(j);
					url = (String) items.get("url");

					label2 = (String) items.get("label");
					target = (String) items.get("target");
					if (target == null)
						target = "";
					if (url != null && !url.equals("")) {
						outputLog.append("<div class = \"label\">" + label1 + " ---" + label2 + "</div>");
						logText = vErrorObj.verifyFor404Errors(url, test_url);
						outputLog.append(logText);
						logText = vTargetObj.verifyTargetWindow(url, test_url, target);
						outputLog.append(logText);
						count++;
					}

				}

			} else {
				url = (String) items.get("url");
				label2 = (String) items.get("label");
				target = (String) items.get("target");
				if (target == null)
					target = "";

				if (url != null && !url.equals("")) {
					outputLog.append("<div class = \"label\">" + label2 + "</div>");
					logText = vErrorObj.verifyFor404Errors(url, test_url);
					outputLog.append(logText);
					logText = vTargetObj.verifyTargetWindow(url, test_url, target);
					outputLog.append(logText);
					count++;
				}

			}

		}
		
	}

	public void allPageComponent(String comp_name, String test_url, WebDriver driver) {
		String url, target, label;
		String xpath_attr = "//" + comp_name;
		if (comp_name.equals("sw-mosaic-static")) {
			xpath_attr = "//" + comp_name + "[" + mosaic_tile_counter + "]";
			mosaic_tile_counter++;
		}

		// added for ptx-series page, it has prod category and sw-notes both on
		// page, so if xpath is only given as sw-notes it takes the sw-notes
		// element under the sw-prod-category
		// to avoid this iterating as given below
		if (comp_name.equals("sw-notes") && prod_cate_counter == 2) {
			xpath_attr = "//main/" + comp_name + "[1]";
		}
		//added for qfx series page with multiple sw-notes component
			else if(comp_name.equals("sw-notes")){
				xpath_attr = "//" + comp_name + "["+ sw_notes_counter+ "]";
				sw_notes_counter++;
			}
		

		if (comp_name.equals("sw-product-category")) {
			xpath_attr = "//" + comp_name + "[" + prod_cate_counter + "]";
			prod_cate_counter++;
		}

		WebElement prod_ovr = driver.findElement(By.xpath(xpath_attr));
		outputLog.append("<h3>" + comp_name + "</h3>");
		List<WebElement> prodovr_links = prod_ovr.findElements(By.tagName("a"));
		for (WebElement e : prodovr_links) {
			url = e.getAttribute("href").toString();

			target = e.getAttribute("target");
			if (comp_name.equals("sw-product-overview"))
				label = e.getText();
			else
				label = e.getAttribute("innerText");

			label = editLabel(label);

			if (url != null && !url.equals("")) {
				outputLog.append("<div class = \"label\">" + label + "</div>");
				logText = vErrorObj.verifyFor404Errors(url, test_url);
				outputLog.append(logText);
				logText = vTargetObj.verifyTargetWindow(url, test_url, target);
				outputLog.append(logText);
				count++;
			}
		}

	}

	private String editLabel(String label) {
		if (label.contains("chevron_right")) {
			label = label.replace("chevron_right", "");
		}
		if (label.contains("play_arrow")) {
			label = label.replace("play_arrow", "");
		}
		if (label.contains("dots_arrow_right")) {
			label = label.replace("dots_arrow_right", "");
		}
		if (label.contains("navigate_next")) {
			label = label.replace("navigate_next", "");
		}
		if (label.contains("navigate_before")) {
			label = label.replace("navigate_before", "");
		}
		return label;
	}

	public void swSubNavComponent(JSONObject properties, String test_url) {
		outputLog.append("<h3>sub navigation</h3>");
		JSONArray swSubNavArray = (JSONArray) properties.get("items");

		for (int i = 0; i < swSubNavArray.size(); i++) {
			JSONObject subnav = (JSONObject) swSubNavArray.get(i);
			subComponent(subnav, test_url);

			JSONArray itemsArray1;
			if ((subnav.get("items")) == null)
				continue;
			else
				itemsArray1 = (JSONArray) subnav.get("items");
			for (int j = 0; j < itemsArray1.size(); j++) {
				JSONObject subitems = (JSONObject) itemsArray1.get(j);
				subComponent(subitems, test_url);

				JSONArray itemsArray2;
				if ((subitems.get("items")) == null)
					continue;
				else
					itemsArray2 = (JSONArray) subitems.get("items");
				for (int k = 0; k < itemsArray2.size(); k++) {
					JSONObject subitems2 = (JSONObject) itemsArray2.get(k);
					subComponent(subitems2, test_url);

				}
			}
		}

	}

	public void swEditorialComponent(JSONObject properties, String test_url, WebDriver driver, String fullPageSrc) {

		// to find number of occurrences of the editorial component
		int sw_count = StringUtils.countMatches(fullPageSrc, "sw-editorial");

		// https://uat.juniper.net/us/en/solutions/cloud/
		/*
		 * if (sw_count == 1) {
		 * outputLog.append("<h3> Editorial component</h3>"); JSONArray
		 * itemsArray = (JSONArray) properties.get("items"); for (int i = 0; i <
		 * itemsArray.size(); i++) { JSONObject itemsObj = (JSONObject)
		 * itemsArray.get(i); JSONObject propObj = (JSONObject)
		 * itemsObj.get("properties"); subComponent(propObj, test_url);
		 * 
		 * } }
		 */
		// added for junos training page
		// https://uat.juniper.net/us/en/training/junos-genius/

		try {

			if (fullPageSrc.contains("sw-editorial")) {

				String xpath_attr = "//sw-editorial[" + editorial_count + "]";
				editorial_count++;
				WebElement editorial = driver.findElement(By.xpath(xpath_attr));
				List<WebElement> editTextLinks = editorial.findElements(By.tagName("a"));
				if (editTextLinks.size() > 0) {
					outputLog.append("<h3> Editorial component</h3>");
					for (WebElement e : editTextLinks) {
						String url = e.getAttribute("href");
						String target = e.getAttribute("target");
						String label = e.getAttribute("innerText");
						label = editLabel(label);
						if (url != null && !url.equals("")) {
							outputLog.append(label);
							logText = vErrorObj.verifyFor404Errors(url, test_url);
							outputLog.append(logText);
							logText = vTargetObj.verifyTargetWindow(url, test_url, target);
							outputLog.append(logText);
							count++;
						}
					}
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	public int getCount() {
		return count;
	}

	public void swTrainingBooksComponent(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3>Training books </h3>");
		String endPoint_url = properties.get("endpoint").toString();

		String start = "";
		if (test_url.startsWith("https://origin-www.juniper.net"))
			start = "https://origin-www.juniper.net";
		else if (test_url.startsWith("https://www.juniper.net"))
			start = "https://www.juniper.net";
		else if (test_url.startsWith("https://origin-stage.juniper.net"))
			start = "https://origin-stage.juniper.net";
		else if (test_url.startsWith("https://stage.juniper.net"))
			start = "https://stage.juniper.net";
		else if (test_url.startsWith("https://uat.juniper.net"))
			start = "https://uat.juniper.net";

		if (endPoint_url.startsWith("/"))
			;
		;
		endPoint_url = start + endPoint_url;
		System.out.println(endPoint_url);

		StringBuilder pageSrc = new StringBuilder();
		try {

			URL sub_url = new URL(endPoint_url);
			BufferedReader br = new BufferedReader(new InputStreamReader(sub_url.openStream(), "UTF-8"));

			String line;
			while ((line = br.readLine()) != null) {
				pageSrc.append(line);
			}

			String fullPageSrc = pageSrc.toString();
			br.close();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(fullPageSrc);
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray dataArray = (JSONArray) jsonObject.get("data");
			for (int i = 0; i < dataArray.size(); i++) {
				JSONObject dataObj = (JSONObject) dataArray.get(i);

				subComponent(dataObj, test_url);

				JSONArray formatsArray = (JSONArray) dataObj.get("formats");
				for (int k = 0; k < formatsArray.size(); k++) {
					JSONObject formatsObj = (JSONObject) formatsArray.get(k);
					subComponent(formatsObj, test_url);

				}

			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void compareProductsPage(JSONObject properties, String test_url, WebDriver driver) {

		outputLog.append("<h3> compare component</h3>");
		JSONArray itemsArray = (JSONArray) properties.get("items");
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject itemsObj = (JSONObject) itemsArray.get(i);
			JSONObject detailsObj = (JSONObject) itemsObj.get("details");
			subComponentCompare(detailsObj, test_url);

		}
	}

	// this is only for sw-compare-product , since url is hosted in page tag
	// instead of url tag.
	private void subComponentCompare(JSONObject items, String test_url) {
		String url, target, label;
		if ((items.get("page")) == null)
			url = "";
		else
			url = items.get("page").toString();

		if ((items.get("target")) == null)
			target = "";
		else
			target = items.get("target").toString();

		if ((items.get("label")) == null)
			label = "";
		else
			label = items.get("label").toString();

		if (url != null && !url.equals("")) {
			outputLog.append(label);
			logText = vErrorObj.verifyFor404Errors(url, test_url);
			outputLog.append(logText);
			logText = vTargetObj.verifyTargetWindow(url, test_url, target);
			outputLog.append(logText);
		}

	}

	public void swMarqueeSlides(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3> sw-marquee-slides</h3>");
		JSONArray itemsArray = (JSONArray) properties.get("items");
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject itemsObj = (JSONObject) itemsArray.get(i);
			JSONObject actionObj = (JSONObject) itemsObj.get("action");
			subComponent(actionObj, test_url);

		}
	}

	public void linkCardsComponent(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3> sw-link-cards</h3>");
		JSONArray itemsArray = (JSONArray) properties.get("items");
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject itemsObj = (JSONObject) itemsArray.get(i);
			JSONArray itemsArray2 = (JSONArray) itemsObj.get("items");
			for (int j = 0; j < itemsArray2.size(); j++) {
				JSONObject itemsObj2 = (JSONObject) itemsArray2.get(j);
				subComponent(itemsObj2, test_url);
			}

			JSONArray relatedArray = (JSONArray) itemsObj.get("related");
			for (int j = 0; j < relatedArray.size(); j++) {
				JSONObject relatedObj = (JSONObject) relatedArray.get(j);
				subComponent(relatedObj, test_url);
			}

		}

	}

	public void swbannerComponent(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3> sw-banner</h3>");
		JSONObject actionObj = (JSONObject) properties.get("action");
		subComponent(actionObj, test_url);
	}

	public void swTableComponent(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3> sw-table</h3>");
		JSONArray itemsArray = (JSONArray) properties.get("items");
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject itemsObj = (JSONObject) itemsArray.get(i);
			for (int k = 0; k < itemsObj.size(); k++) {
				JSONObject colObj = (JSONObject) itemsObj.get("col_" + k);
				JSONArray itemsArray2 = (JSONArray) colObj.get("items");
				for (int j = 0; j < itemsArray2.size(); j++) {
					JSONObject subObj = (JSONObject) itemsArray2.get(j);
					JSONObject propObj = (JSONObject) subObj.get("properties");
					subComponent(propObj, test_url);
				}
			}
		}
	}

	public void swDualInfoComponent(JSONObject properties, String test_url, WebDriver driver) {
		outputLog.append("<h3> sw-dual-info</h3>");

		// left component
		JSONObject leftObj = (JSONObject) properties.get("left");
		JSONObject actionObj = (JSONObject) leftObj.get("action");
		subComponent(actionObj, test_url);

		// right component
		JSONObject rightObj = (JSONObject) properties.get("right");
		JSONArray itemsArray = (JSONArray) rightObj.get("items");
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject subObj = (JSONObject) itemsArray.get(i);
			subComponent(subObj, test_url);
		}

	}

	public int getFail_count_target() {
		return fail_count_target;
	}

	public void setFail_count_target(int fail_count_target) {
		this.fail_count_target = fail_count_target;
	}

	public int getFail_404errors() {
		return fail_404errors;
	}

	public void setFail_404errors(int fail_404errors) {
		this.fail_404errors = fail_404errors;
	}

	public int getManualVerificationCount() {
		return manual_verification_count;
	}

	public void getErrorCount() {
		fail_404errors = vErrorObj.get404Count();
		fail_count_target = vTargetObj.getTargetCount();
		manual_verification_count = vTargetObj.getManualVerificationCount();
	}

	public StringBuilder getOutputLog() {
		return outputLog;
	}

	public void setOutputLog(StringBuilder outputLog) {
		this.outputLog = outputLog;
	}

}
