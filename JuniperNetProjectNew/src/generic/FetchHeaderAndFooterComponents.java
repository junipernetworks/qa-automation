package generic;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import fetchUrls.FetchAllJSONComponents;
import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class FetchHeaderAndFooterComponents {
	// This class is for fetching and verifying the urls present in the header
	// and footer component
	// of juniper.net pages
	static int count;
	StringBuilder outputLog = new StringBuilder();
	String logText = "";
	private  int fail_count_target =0;
	private int fail_404errors = 0;
	private int manual_verification_count = 0;
	VerifyHttpErrors vError = new VerifyHttpErrors();
	VerifyTargetForLinks vTarget = new VerifyTargetForLinks();

	public void fetchGlobalComponents(String json_src, String url) {
		JSONParser parser = new JSONParser();

		try {

			JSONObject compObj;
			Object obj = parser.parse(json_src);
			JSONObject jsonObject = (JSONObject) obj;

			JSONObject glblComponents = (JSONObject) jsonObject.get("globalComponents");
			JSONObject navigation = (JSONObject) glblComponents.get("navigation");
			JSONObject properties = (JSONObject) navigation.get("properties");

			// for Juniper logo
			outputLog.append("<h3>Juniper Logo</h3>");
			JSONObject logoComponent = (JSONObject) properties.get("logo");
			headerFooterSubComponent(logoComponent, url);

			// for header urls
			outputLog.append("<h3>Flyout Navigation components</h3>");
			JSONArray outer_items = (JSONArray) properties.get("items");
			headerComponent(outer_items, url);

			// for footer urls
			JSONObject footerComponents = (JSONObject) glblComponents.get("footer");
			JSONObject ftProperties = (JSONObject) footerComponents.get("properties");

			outputLog.append("<h3>Footer components</h3>");
			JSONArray footerArray = (JSONArray) ftProperties.get("top");
			footerComponent(footerArray, url);

			JSONArray bottomArray = (JSONArray) ftProperties.get("bottom");
			for (int i = 0; i < bottomArray.size(); i++) {
				footerArray = (JSONArray) bottomArray.get(i);
				footerComponent(footerArray, url);

			}

			footerArray = (JSONArray) ftProperties.get("social");
			footerComponent(footerArray, url);

			footerArray = (JSONArray) ftProperties.get("countries");
			footerComponent(footerArray, url);
			
			printOutput();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void footerComponent(JSONArray footerArray, String test_url) {

		for (int i = 0; i < footerArray.size(); i++) {
			JSONObject topObj = (JSONObject) footerArray.get(i);
			headerFooterSubComponent(topObj, test_url);
			
		}

	}

	private void headerComponent(JSONArray itemsArray, String test_url) {
		String url;
		String target;
		String label;
		JSONArray innerItem, innerItem2, innerItem3, innerItem4, innerItem5;
		
		for (int i = 0; i < itemsArray.size(); i++) {
			JSONObject items = (JSONObject) itemsArray.get(i);
			headerFooterSubComponent(items, test_url);
			if ((items.get("items")) == null)
				continue;
			else {
				innerItem = (JSONArray) items.get("items");
				for (int j = 0; j < innerItem.size(); j++) {
					JSONObject item1 = (JSONObject) innerItem.get(j);
					headerFooterSubComponent(item1, test_url);
					if ((item1.get("items")) == null)
						continue;
					else {
						innerItem2 = (JSONArray) item1.get("items");
						for (int k = 0; k < innerItem2.size(); k++) {
							JSONObject item2 = (JSONObject) innerItem2.get(k);
							headerFooterSubComponent(item2, test_url);
							if ((item2.get("items")) == null)
								continue;
							else {
								innerItem3 = (JSONArray) item2.get("items");
								for (int m = 0; m < innerItem3.size(); m++) {
									JSONObject item3 = (JSONObject) innerItem3.get(m);
									headerFooterSubComponent(item3, test_url);
									if ((item3.get("items")) == null)
										continue;
									else {
										innerItem4 = (JSONArray) item3.get("items");
										for (int n = 0; n < innerItem3.size(); n++) {
											JSONObject item4 = (JSONObject) innerItem4.get(n);
											headerFooterSubComponent(item4, test_url);
											if ((item4.get("items")) == null)
												continue;
											else {
												innerItem5 = (JSONArray) item4.get("items");
												for (int p = 0; p < innerItem5.size(); p++) {
													JSONObject item5 = (JSONObject) innerItem5.get(p);
													headerFooterSubComponent(item5, test_url);
												}
											}
										}
									}
								}
							}
						}

					}
				}
			}
		}
		
	}
	
	

	public void headerFooterSubComponent(JSONObject items, String test_url) {
		String url;
		String target;
		String label;
		JSONArray inner_item = null;

		if ((items.get("url")) == null)
			url = "";
		else
			url = items.get("url").toString();

		if ((items.get("target")) == null)
			target = "";
		else
			target = items.get("target").toString();

		if ((items.get("label")) == null)
			label = "";
		else
			label = items.get("label").toString();

		outputLog.append(label);
		logText = vError.verifyFor404Errors(url, test_url);
		outputLog.append(logText);
		logText = vTarget.verifyTargetWindow(url, test_url, target);
		outputLog.append(logText);
		count++;
		
	}

	public  void oldPageComponent(String eachUrl, WebDriver driver) {
		WebElement headerComponent = driver.findElement(By.xpath("//header[@class = 'main-header ']"));
		List<WebElement> HeaderLinks = headerComponent.findElements(By.tagName("a"));

		WebElement footerComponent = driver.findElement(By.xpath("//section[@class = 'footer']"));
		List<WebElement> footerLinks = footerComponent.findElements(By.tagName("a"));

		int total_links = HeaderLinks.size() + footerLinks.size();
		outputLog.append("<div class=\"count\">" + "Total number of links - " + total_links + "</div>");

		outputLog.append("<h3Header/Flyout navigaton links</h3>");
		fetchLinks(HeaderLinks, eachUrl);
		
		outputLog.append("<h3>"+ "Footer links" +"</h3>");
		fetchLinks(footerLinks, eachUrl);
		
		printOutput();
		
	}

	private  void fetchLinks(List<WebElement> links, String test_url) {

		String url = "";
		String target = "";
		String label = "";
		String title = "";

		for (WebElement e : links) {
			url = e.getAttribute("href");
			target = e.getAttribute("target");
			label = e.getAttribute("innerText");
			if(label==null){
				title = e.getAttribute("label");
			}
			outputLog.append(title);
			outputLog.append(label);
			logText = vError.verifyFor404Errors(url, test_url);
			outputLog.append(logText);
			logText = vTarget.verifyTargetWindow(url, test_url, target);
			outputLog.append(logText);
			
		}

	}
	
	public void printOutput(){
		
		getErrorCount();
		
		Reporter.log("<div class=\"count\">" + "Number of urls: " + getCount() + "</div>");
		
		Reporter.log("<div class=\"total\">" +"Total 404 issues on page = "+ getFail_404errors()+ "</div>");
		
		Reporter.log("<div class=\"total\">" +"Total target link failures on page = " + getFail_count_target()+ "</div>");
		
		Reporter.log("<div class=\"total\">" +"Total number of links to be verified manually = "+ getManualVerificationCount()+ "</div>");
		
		//StringBuilder outputLog = hdrfdr.getOutputLog();
		Reporter.log(getOutputLog().toString());
	}

	public int getCount() {
		
		return count;
	}

	public int getFail_count_target() {
		return fail_count_target;
	}

	public void setFail_count_target(int fail_count_target) {
		this.fail_count_target = fail_count_target;
	}

	public int getFail_404errors() {
		return fail_404errors;
	}

	public void setFail_404errors(int fail_404errors) {
		this.fail_404errors = fail_404errors;
	}
	
	public int getManualVerificationCount() {
		return manual_verification_count;
	}

	
	public void getErrorCount(){
		fail_404errors = vError.get404Count();
		fail_count_target = vTarget.getTargetCount();
		manual_verification_count = vTarget.getManualVerificationCount();
	}

	public StringBuilder getOutputLog() {
		return outputLog;
	}

	public void setOutputLog(StringBuilder outputLog) {
		this.outputLog = outputLog;
	}
}
