package generic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import fetchUrls.FetchAllUrls;
import junit.framework.Assert;
import utility.VerifyHttpErrors;
import utility.VerifyTargetForLinks;

public class TestHeaderFooter extends scripts.BaseTest {
	boolean firstTime = true;

	@Test
	public void readInputFile() {
		BufferedReader br = null;
		int numberOfLines = 0;
		String eachUrl;
		String entirePageSrc;
		String workingDir = System.getProperty("user.dir");
		
		try {
			File file = new File("C:\\Juniper.NetAutomation\\Input\\ListOfUrls.txt");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String sCurrentLine;

			if ((file.length()) == 0) {
				Reporter.log("No urls present in file, enter the urls to test", true);
			}

			ArrayList<String> urlsToTest = new ArrayList<String>();

			while ((sCurrentLine = br.readLine()) != null) {

				if (sCurrentLine.isEmpty())
					continue;

				urlsToTest.add(sCurrentLine.trim());
				numberOfLines++;

			}
			System.out.println("number of urls to test" + numberOfLines);

			Iterator itr = urlsToTest.iterator();

			Reporter.log("<br>");
			Reporter.log("<div class=\"container\">");

			Reporter.log("<div class=\"tabs\" data-tabsize=\"" + urlsToTest.size() + "\"></div>");
			int count = 0;
			Reporter.log("<div class=\"tabBody\">");
			while (itr.hasNext()) {
				Reporter.log("<div class=\"page\" data-pageid=\"pg" + (++count) + "\">");
				eachUrl = itr.next().toString().trim();
				Reporter.log("<div class=\"msgTitle\">" + "<a href=" + eachUrl + " target=\"_blank\">" + "Url: "
						+ eachUrl + "</a></div>");
				Reporter.log("<br>");
				VerifyHttpErrors httperror = new VerifyHttpErrors();
				String flag = httperror.verifyFor404Errors(eachUrl, eachUrl);

				if (flag.equalsIgnoreCase("test url leads to 404 page not found error"))
					break;

				VerifyTargetForLinks targeterrors = new VerifyTargetForLinks();
				targeterrors.verifyTargetWindow(eachUrl, eachUrl, "");
				driver.get(eachUrl);
				System.out.println(eachUrl);
				getPagesource(eachUrl);
				Reporter.log("</div>");

			}
			Reporter.log("</div>");
			Reporter.log("</div>");
			if (firstTime) {
				Reporter.log(
						"<script type=\"text/javascript\" src=\"https://www.juniper.net/documentation/test/POC/dme-qa/scripts/jquery.min.js\"></script>");

				// To add css and javascript to output html file
				outputhtmlformat.CSSandJavaScript.AddCssAndJStoOutputHTMLFile("/styles/cssStyles.txt");
				outputhtmlformat.CSSandJavaScript.AddCssAndJStoOutputHTMLFile("/javascript/jsJavaScript.txt");
				firstTime = !firstTime;
			}

		} catch (WebDriverException e) {
			String errMsg = e.getMessage();
			if (errMsg.contains("chrome not reachable")
					|| errMsg.contains("no such window: target window already closed")) {
				System.out.println("Browser was closed by user during execution..try again");
			} else
				System.out.println(e.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void getPagesource(String eachUrl) throws WebDriverException {

		StringBuilder pageSrc = new StringBuilder();
		String line;
		String pageData = null;
		System.out.println("inside page source");
		if (eachUrl.contains("http:"))
			;
		eachUrl = eachUrl.replace("http:", "https:");
		FetchHeaderAndFooterComponents fhafc = new FetchHeaderAndFooterComponents();
		FetchHeaderAndFooterComponents hdrfdr = new FetchHeaderAndFooterComponents();

		try {
			URL url = new URL(eachUrl);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

			while ((line = br.readLine()) != null) {
				pageSrc.append(line);
			}

			String fullPageSrc = pageSrc.toString();
			br.close();
			// is.close();

			String json_tag = "_PAGE_DATA =";

			if (fullPageSrc.contains(json_tag)) {
				pageData = pageSrc.substring(pageSrc.indexOf("_PAGE_DATA") + json_tag.length(),
						pageSrc.indexOf("};") + 1);
				fhafc.fetchGlobalComponents(pageData, eachUrl);
			}

			else {
				pageData = fullPageSrc;
				fhafc.oldPageComponent(eachUrl, driver);
			}

		} catch (MalformedURLException e) {
			Reporter.log(eachUrl + "url is malformed");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			Reporter.log("Invalid url or hostname");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
